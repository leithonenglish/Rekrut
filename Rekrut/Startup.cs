﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rekrut.Startup))]
namespace Rekrut
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
