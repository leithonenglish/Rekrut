﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rekrut.Models.Entities
{
    [BsonIgnoreExtraElements]
	public class Task
	{
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DateAssigned { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime StartDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime EndDate { get; set; }
        [BsonIgnoreIfNull]
        public DateTime? CompletionDate { get; set; }
        public bool ForDayEnd { get; set; }
        public bool ForDayBegining { get; set; }
        public string Project { get; set; }
        public string Source { get; set; }
        public string AssignedBy { get; set; }
        public int Priority { get; set; }
        public int AssignedTo { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> Files { get; set; }
    }
}