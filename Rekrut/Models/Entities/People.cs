﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Rekrut.Models.Entities
{
    [BsonIgnoreExtraElements]
    public class People
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Username { get; set; }
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Img { get; set; }
        public string JobTitle { get; set; }
        public int Department { get; set; }
        public string Email { get; set; }
        public int Extension { get; set; }
        public string Phone { get; set; }
        public Address Address { get; set; }
        public bool HasFamily { get; set; }
        public string Religion { get; set; }
        [BsonIgnoreIfNull]
        public List<string> Motivations { get; set; }
        [BsonIgnoreIfNull]
        public List<string> PreferedWorkingConditons { get; set; }
        [BsonIgnoreIfNull]
        public List<Credential> Credentials { get; set; }
        [BsonIgnoreIfNull]
        public string Personality { get; set; }
        [BsonIgnoreIfNull]
        public List<string> Strengths { get; set; }
        [BsonIgnoreIfNull]
        public List<string> Weaknesses { get; set; }
        [BsonIgnoreIfNull]
        public List<PreviousPostion> PreviousPositions { get; set; }
        [BsonIgnoreIfNull]
        public List<string> Interests { get; set; }
        [BsonIgnoreIfNull]
        public List<string> PreferedTasks { get; set; }
        [BsonIgnoreIfNull]
        public string CareerPath { get; set; }
        [BsonIgnoreIfNull]
        public List<Familiar> Tools { get; set; }
        [BsonIgnoreIfNull]
        public List<Familiar> Languages { get; set; }
        [BsonIgnoreIfNull]
        public List<Familiar> Skills { get; set; }
        public decimal YearsOfExperience { get; set; }
    }

    public class Address
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
    }

    public class Credential
    {
        public string Type { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local, DateOnly = true)]
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Institution { get; set; }
    }

    public class PreviousPostion
    {
        public string Name { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
    [BsonIgnoreExtraElements]
    public class Familiar
    {
        [BsonIgnoreIfNull]
        public string Name { get; set; }
        [BsonIgnoreIfNull]
        public int Rating { get; set; }
        [BsonIgnoreIfNull]
        public decimal Experience { get; set; }
    }
}