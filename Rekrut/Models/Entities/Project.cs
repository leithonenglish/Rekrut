﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Rekrut.Models.Entities
{
    [BsonIgnoreExtraElements]
    public class Project
	{
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonIgnoreIfNull]
        public List<string> Links { get; set; }
        [BsonIgnoreIfNull]
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Manager { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Client { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        [BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }
        public int Priority { get; set; }
        public string Difficulty { get; set; }
    }
    public class Team
    {
        public List<TeamMember> Members { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class TeamMember
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
    }
}