﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rekrut.Models
{
    public class SkillPoolFilter
    {
        public int? DepartmentId { get; set; }
        public int ExperienceFrom { get; set; }
        public int ExperienceTo { get; set; }
        public bool Everyone { get; set; }
        public bool HasFamily { get; set; }
        public bool IsReligious { get; set; }
        public bool OnProject { get; set; }
        public List<string> Tags { get; set; }
    }
    public class ProjectFilter
    {
        public bool assignedOnly { get; set; }
        public List<string> difficulties { get; set; }
        public List<string> types { get; set; }
        public List<string> statuses { get; set; }
        public List<int> priorities { get; set; }
    }
}