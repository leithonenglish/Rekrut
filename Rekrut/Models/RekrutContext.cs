﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using Rekrut.Models.Entities;

namespace Rekrut.Models
{
    public class RekrutContext
    {
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;
        private static readonly GridFSBucket _gridFS;

        static RekrutContext()
        {
            var conventionPack = new ConventionPack();
            conventionPack.Add(new CamelCaseElementNameConvention());
            ConventionRegistry.Register("camelCase", conventionPack, t => true);
            var url = new MongoUrl(System.Configuration.ConfigurationManager.ConnectionStrings["RekrutUrl"].ConnectionString);
            _client = new MongoClient(url);
            _database = _client.GetDatabase(url.DatabaseName);
            _gridFS = new GridFSBucket(_database);
        }

        public IMongoDatabase Database
        {
            get { return _database; }
        }

        public IMongoClient Client
        {
            get { return _client; }
        }

        public GridFSBucket GridFS
        {
            get { return _gridFS; }
        }

        //entities
        public IMongoCollection<People> People
        {
            get { return _database.GetCollection<People>("people"); }
        }

        public IMongoCollection<Department> Departments
        {
            get { return _database.GetCollection<Department>("departments"); }
        }

        public IMongoCollection<Project> Projects
        {
            get { return _database.GetCollection<Project>("projects"); }
        }

        public IMongoCollection<Task> Tasks
        {
            get { return _database.GetCollection<Task>("tasks"); }
        }

    }
}