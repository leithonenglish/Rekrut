﻿using System;
using System.Text;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Rekrut.Models.Reusables
{
	public static class Extensions
	{
        public static void AddInArrayFilter<T>(this BsonArray source, string field, List<T> array)
        {
            var bsonArray = new BsonArray();
            foreach (var val in array)
            {
                if (typeof(T) == typeof(string))
                    bsonArray.Add(new Regex(val.ToString(), RegexOptions.IgnoreCase));
                else
                    bsonArray.Add(Convert.ToInt16(val));
            }
            source.Add(new BsonDocument(field, new BsonDocument("$in", bsonArray)));
        }
        public static int DaysLeft(this DateTime date)
        {
            return  date.DayOfYear - DateTime.Now.DayOfYear;
        }
        public static TimeSpan ToTimeSpan(this string timeString)
        {
            var dt = DateTime.ParseExact(timeString, "h:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            return dt.TimeOfDay;
        }
        public static string NormalizeSentence(this string source, bool uppCaseFirstLetterAfterSpace = true, bool forceToLower = true)
        {
            var chars = source.ToCharArray();
            StringBuilder str = new StringBuilder();
            for (int x = 0; x < chars.Length; x++)
            {
                string character = chars[x].ToString();
                char ch = chars[x];
                if (!string.IsNullOrEmpty(character) && chars.Length >= 3 && ch != '.' && ch != ' ')
                {
                    if (x == 0 ||
                        (x > 0 && Char.IsLetter(ch) && (x + 2 <= chars.Length &&
                        ((chars[x + 1] == '.' && Char.IsLetter(chars[x + 2]))) ||
                        chars[x - 1] == '.' || (chars[x - 1] == ' ' && uppCaseFirstLetterAfterSpace) ||
                        (x > 1 && source.Substring(x - 2, 2) == ". "))))
                    {
                        str.Append(character.ToUpper());
                        continue;
                    }
                }
                str.Append(forceToLower ? character.ToLower() : character);
            }
            return str.ToString();
        }
    }
}