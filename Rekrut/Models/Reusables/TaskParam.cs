﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Rekrut.Models.Reusables
{
    public class TaskParam
    {
        public string projectId { get; set; }
        public string projectName { get; set; }
        public List<TaskParamTeamMember> members { get; set; }
    }
    public class TaskParamTeamMember
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class TaskViewModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string source { get; set; }
        public string project { get; set; }
        public int priority { get; set; }
        public int assignedTo { get; set; }
        public TaskDateViewModel start { get; set; }
        public TaskDateViewModel end { get; set; }
    }
    public class TaskDateViewModel
    {
        public string date { get; set; }
        public string time { get; set; }
        public bool timeOn { get; set; }
    }
    public class TaskFilterModel
    {
        public bool closed { get; set; }
        public bool active { get; set; }
        public bool completed { get; set; }
        public bool overdue { get; set; }
        public bool processing { get; set; }
    }
    public class TaskStatusSummary
    {
        public string id { get; set; }
        public int count { get; set; }
    }
}