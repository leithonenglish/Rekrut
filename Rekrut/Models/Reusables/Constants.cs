﻿using System.Collections.Generic;

namespace Rekrut.Models.Reusables
{
	public static class Constants
	{
        public static Dictionary<string, string> TaskSource = new Dictionary<string, string>
        {
            {"dev", "Developement" },
            {"qa", "Quality Assurance" },
            {"prod", "Production" }
        };
        public static Dictionary<string, string> TaskStatus = new Dictionary<string, string>
        {
            {"a", "Active" },
            {"d", "Completed" },
            {"c", "Closed" },
            {"o", "Overdue" },
            {"p", "Processing" }
        };
        public static Dictionary<int, string> TaskPriority = new Dictionary<int, string>
        {
            {0, "None" },
            {1, "Low" },
            {2, "Medium" },
            {3, "High" },
        };
        #region Project
        public static Dictionary<int, string> ProjectPriority = new Dictionary<int, string>
        {
            {0, "None" },
            {1, "Low" },
            {2, "Medium" },
            {3, "High" },
            {4, "Critical" },
        };
        public static Dictionary<string, string> ProjectStatus = new Dictionary<string, string>
        {
            {"o", "Ongoing" },
            {"m", "Maintained" },
            {"n", "New" },
            {"c", "Closed" }
        };
        public static Dictionary<string, string> ProjectType = new Dictionary<string, string>
        {
            {"o", "One Time" },
            {"c", "Contracted" },
            {"f", "Fix" },
            {"l", "Linked" }
        };
        public static Dictionary<string, string> ProjectDifficulty = new Dictionary<string, string>
        {
            {"e", "Easy" },
            {"n", "Normal" },
            {"m", "Medium" },
            {"h", "Hard" },
            {"x", "Extreme" }
        };
        #endregion
    }
}