﻿using System.Web;
using System.Web.Optimization;

namespace Rekrut
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.easing.1.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/bootstrap-timepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/components").Include(
                      "~/Scripts/perfect-scrollbar.js",
                      "~/Scripts/Chart.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/select2.js",
                      "~/Scripts/imagesloaded.js",
                      "~/Scripts/masonry.js"));

            bundles.Add(new ScriptBundle("~/bundles/rekrut/angular").Include("~/Scripts/rekrut-angular/rekrut.angular.js")
                .IncludeDirectory("~/Scripts/rekrut-angular/ctrls", "*.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ui").IncludeDirectory("~/Scripts/angular-ui", "*.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include("~/Scripts/angular.js", "~/Scripts/angular-*"));

            bundles.Add(new StyleBundle("~/Content/css/global").IncludeDirectory("~/Content/css/global", "*.css"));
            bundles.Add(new StyleBundle("~/Content/css/global/bootstrap").IncludeDirectory("~/Content/css/global/bootstrap", "*.css"));
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/css/rekrut.css"));

        }
    }
}
