﻿var ctrls = {};
var rekrutApp = angular.module("rekrut", ['ngRoute', 'ui.bootstrap', 'ngResource', 'ui.checkbox', 'rzModule', 'ui.select', 'ngSanitize', 'angularFileUpload', 'wu.masonry']);

rekrutApp.directive('clickOut', ['$window', '$parse', function ($window, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var clickOutHandler = $parse(attrs.clickOut);

            angular.element($window).on('click mousewheel blur scroll', function (event) {
                if (element[0].contains(event.target)) return;
                clickOutHandler(scope, { $event: event });
                scope.$apply();
            });
        }
    };
}]);
rekrutApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            title: "Rekrüt | Dashboard",
            templateUrl: 'dashboard',
            controller: 'dashboardCtrl'
        })
        .when("/skilldirectory", {
            title: "Rekrüt | Skill Directory",
            templateUrl: 'skillDirectory',
            controller: 'skillDirectoryCtrl'
        })
        .when("/taskbriefs", {
            title: "Rekrüt | Task Briefs",
            templateUrl: 'taskBriefs',
            controller: 'taskBriefsCtrl'
        })
        .when("/projects", {
            title: "Rekrüt | Projects",
            templateUrl: 'projects',
            controller: 'projectsCtrl'
        })
        .when("/applications", {
            title: "Rekrüt | Applicaitons",
            templateUrl: 'applications',
            controller: 'applicationsCtrl'
        })
        .otherwise({ redirectTo: '/' });
});
rekrutApp.run(function ($location, $route, $rootScope, $http, $compile, $timeout) {
    $rootScope.$route = $route;
    $rootScope.url = "";
    $rootScope.activePath = null;
    $rootScope.modal = false;
    $rootScope.person = null;
    $rootScope.projects = 0;
    $rootScope.tasks = 0;
    
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        $rootScope.pageLoading = true;
        $rootScope.modal = true;
    });
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.activePath = $location.path();
        $rootScope.title = current.$$route.title;
        $timeout(function () {
            $rootScope.modal = false;
            $rootScope.pageLoading = false;
        }, 500);
    });
});

//jQuery
$(document).on("click", ".upload-button button", function (e) {
    e.preventDefault();
    $(this).parent().find("input:file").trigger("click");
});

//functions
Array.prototype.findByValue = function (prop, val, valProp) {
    var elm = this.filter(function (v, i, a) {
        return v[prop] == val;
    })[0];
    if (typeof elm !== 'undefined') {
        if (typeof valProp !== 'undefined')
            return elm[valProp];
        return elm;
    }
    return null;
}
Array.prototype.angularIndexOf = function (obj) {
    for (var i = 0; i < this.length; i++) {
        if (angular.equals(this[i], obj)) {
            return i;
        }
    };
    return -1;
}
Array.prototype.idIndexOf = function (field, id) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][field] === id) {
            return i;
        }
    };
    return -1;
}
var propsHasValueCount = function (obj, val) {
    var count = 0;
    for (var prop in obj) {
        var p = obj[prop];
        if (typeof p !== 'undefined' && p != null) {
            if (p == val)
                count++;
        }
    }
    return count;
}
var objectHasProperties = function (obj) {
    if (typeof obj === 'object') {
        for (var prop in obj) {
            return true;
        }
    }
    return false;
}
var createFormDateFromObject = function (formData, obj, field) {
    for (var prop in obj) {
        if (objectHasProperties(obj[prop])) {
            createFormDateFromObject(formData, obj[prop], prop);
        }
        else {
            formData.append((typeof field === 'undefined' ? prop : field + '.' + prop), obj[prop]);
        }
    }
    return formData;
}