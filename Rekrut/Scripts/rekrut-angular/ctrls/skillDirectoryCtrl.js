﻿ctrls.skillDirectoryCtrl = function ($scope, $rootScope, $http, $timeout) {
    //functions
    $scope.inIt = function () {
        $http.get('/skillDirectory/getDepartmentSummary').then(function (response) {
            var total = 0;
            for(var x = 0; x < response.data.length; x++)
                total += response.data[x].count;

            response.data.unshift({ id: -1, name: "All Departments", count: total });
            $scope.applicableDepartments = response.data;
        });
    }
    $scope.addTag = function () {
        if ($scope.newTag.trim().length > 0) {
            $scope.tags.push($scope.newTag);
            $scope.newTag = "";
            $scope.filterChange();
        }
    };
    $scope.deleteTag = function (index) {
        $scope.tags.splice(index, 1);
        $scope.filterChange();
    }
    $scope.reset = function () {
        $scope.newTag = "";
        $scope.tags = [];
        $scope.people = [];
        $scope.selectedDepartment = null;
        $scope.fromASearch = false;
        $scope.loading = false;
        $scope.filter = {
            experience: { min: 5, max: 20 },
            everyone: true,
            onProject: false,
            hasFamily: false,
            isReligious: false
        }
    }
    $scope.selectDepartment = function (id) {
        $scope.selectedDepartment = id;
        $scope.filterChange();
    }
    $scope.sliderOptions = {
        floor: 0,
        ceil: 30,
        hideLimitLabels: true,
        onEnd: function () { $scope.filterChange() },
        noSwitching: true
    };
    $scope.filterChange = function () {
        $scope.comparedItems = [];
        $scope.comparing = false;
        if ($scope.filter.everyone) {
            $scope.filter.hasFamily = false;
            $scope.filter.onProject = false;
            $scope.filter.isReligious = false;
        }
        $scope.loading = true;
        var data = {
            departmentId: $scope.selectedDepartment,
            experienceFrom: $scope.filter.experience.min,
            experienceTo: $scope.filter.experience.max,
            hasFamily: $scope.filter.hasFamily,
            everyone: $scope.filter.everyone,
            isReligious: $scope.filter.isReligious,
            onProject: $scope.filter.onProject,
            tags: $scope.tags
        }
        $http.get('/skillDirectory/getPeople', { params: data }).then(function (response) {
            $scope.people = response.data;
            $scope.fromASearch = true;
            $timeout(function () {
                $scope.loading = false;
            }, 1000);
            $timeout(function () {
                $('#searchResults .people-list > li .card-reveal > ul').perfectScrollbar();
                $('#searchResults').perfectScrollbar('update');
            }, 1000);
        });
    }
    $scope.viewProfile = function (id) {
        $rootScope.modal = true;
        $http.post('/people/getPerson', { employeeId: id }).then(function (response) {
            $rootScope.personCard = response.data;
            $rootScope.url = "personCard.html";
        });
    };
    $scope.toggleComparing = function () {
        $scope.comparing = !$scope.comparing;
        if (!$scope.comparing) {
            $scope.comparedItems = [];
        }
    }
    $scope.addOrRemCompare = function (employeeId) {
        var index = $scope.comparedItems.indexOf(employeeId);
        if (index == -1 && $scope.comparedItems.length > 1) {
            $scope.comparedItems[1] = employeeId;
        }
        else {
            if (index > -1)
                $scope.comparedItems.splice(index, 1);
            else
                $scope.comparedItems.push(employeeId);
        }
    }

    //varibales
    $scope.reset();
    $scope.comparedItems = [];
    $scope.comparing = false;
    $scope.loading = false;
    $scope.fromASearch = false;
    $scope.applicableDepartments = [];
    $scope.sortBy = "firstName";
    $scope.selectedDepartment = null,
    $scope.expText = function () {
        var expBegining = $scope.filter.experience.min;
        var expEnd = $scope.filter.experience.max;
        if (expBegining == expEnd)
            return expBegining == 0 ? "Less than a year" : "Exactly " + expBegining + " " + (expBegining > 1 ? " years" : "year");
        else
            return expBegining == 0 ? "Less than or equal to " + expEnd + (expEnd == 1 ? " year" : " years") :
                "From " + expBegining + " to " + expEnd + " years";
    };
    $scope.sorts = [
        { value: "firstName", name: "First Name" },
        { value: "lastName", name: "Last Name" },
        { value: "jobTitle", name: "Job Title" },
        { value: "experience", name: "Experience" },
    ];

    $scope.inIt();
}