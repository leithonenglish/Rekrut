﻿ctrls.projectsCtrl = function ($scope, $rootScope, $http, $timeout, $compile) {
    $scope.projects = []
    $scope.reset = function () {
        $scope.people = [];
        $scope.search = "";
        $scope.searching = false;
        $scope.addedPeople = [];
        $scope.filter = {
            types: [],
            difficulties: [],
            statuses: [],
            priorities: [],
            assignedOnly: false
        }
        $scope.sorts = [
            { value: "name", name: "Name" },
            { value: "status", name: "Status" },
            { value: "type", name: "Type" },
            { value: "team", name: "Apps" },
            { value: "difficulty", name: "Difficulty" },
            { value: "priority", name: "Priority" }
        ];
        $scope.statuses = [
            { value: "o", name: "Ongoing", checked: false },
            { value: "m", name: "Maintained", checked: false },
            { value: "n", name: "New", checked: false },
            { value: "c", name: "Closed", checked: false }
        ];
        $scope.difficulties = [
            { value: "e", name: "Easy", checked: false },
            { value: "n", name: "Normal", checked: false },
            { value: "m", name: "Medium", checked: false },
            { value: "h", name: "Hard", checked: false },
            { value: "x", name: "Extreme", checked: false }
        ]
        $scope.types = [
            { value: "o", name: "One Time", checked: false },
            { value: "c", name: "Contracted", checked: false },
            { value: "f", name: "Fix", checked: false },
            { value: "l", name: "Linked", checked: false }
        ]
        $scope.priorities = [
            { name: "Critical", value: 4, checked: false },
            { name: "High", value: 3, checked: false },
            { name: "Medium", value: 2, checked: false },
            { name: "Low", value: 1, checked: false },
            { name: "None", value: 0, checked: false },
        ];
        $scope.fields = [
            { name: "First Name", value: "firstName" },
            { name: "Last Name", value: "lastName" },
            { name: "Job Title", value: "jobTitle" },
            { name: "Skills", value: "skills" },
            { name: "Department", value: "department" },
        ]
        $scope.sortBy = $scope.sorts[0].value;
        $scope.field = $scope.fields[0];
        $scope.sortOrder = "asc";
        $scope.project = {
            title: "",
            priority: $scope.priorities[0].value,
            status: $scope.statuses[0].value,
            difficulty: $scope.difficulties[0].value,
            type: $scope.types[0].value
        };
        $scope.sort = 'lastName';
        $scope.reverse = true;
    }
    $scope.reset();
    $scope.resetFilters = function () {
        $scope.filter = {
            types: [],
            difficulties: [],
            statuses: [],
            priorities: [],
            assignedOnly: false
        }
        $scope.projects = [];
    };
    $scope.filterChange = function () {
        $rootScope.getUser(function () {
            $http.post('/projects/getprojects', {
                employeeId: $rootScope.person.employeeId,
                sortby: $scope.sortBy, order: $scope.sortOrder,
                param: $scope.filter
            }).then(function (response) {
                $scope.projects = response.data;
                $rootScope.modal = false;
                $rootScope.pageLoading = false;
            });
        });
    }
    $scope.toggleFilterArray = function(attr, elm){
        var index = $scope.filter[attr].indexOf(elm.value);
        if (index > -1)
            $scope.filter[attr].splice(index, 1);
        else
            $scope.filter[attr].push(elm.value);
        $scope.filterChange();
    }
    $scope.createOrUpdateProject = function (id) {
        $rootScope.modal = true;
        $http.get("/projects/createOrUpdate").success(function (html) {
            $("body").append($compile(html)($scope));
            $('.rekrut-modal select').select2({ minimumResultsForSearch: -1, dropdownCssClass: "rekrut-modal-select" });
            $(".rekrut-modal .input-daterange").datepicker({ format: 'dd/mm/yyyy' });
            $('.rekrut-modal form .inner').perfectScrollbar();
            CKEDITOR.replace('description');
        });
    }
    $scope.searchChange = function () {
        if ($scope.search.length > 0) {
            $scope.searching = true;
            $http.post("/people/GetPeopleForProject", { search: $scope.search, field: $scope.field.value })
                .success(function (people) {
                    $scope.people = people.map(function (person) {
                        if ($scope.addedPeople.idIndexOf("employeeId", person.employeeId) > -1)
                            person.added = true;
                        return person;
                    });
                    $timeout(function () {
                        $scope.searching = false;
                        $('.blocks .selectable-list').perfectScrollbar();
                    }, 500);
                });
        }
        else
            $scope.people = [];
    };
    $scope.closeProjectScreen = function (event, form) {
        $rootScope.closeRekrutModal(event, form, function () {
            $scope.reset(false);
        });
    };
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.sort === predicate) ? !$scope.reverse : false;
        $scope.sort = predicate;
    };
    $scope.addOrRemove = function (person) {
        var index = $scope.addedPeople.idIndexOf("employeeId", person.employeeId);;
        if (index > -1)
            $scope.addedPeople.splice(index, 1);
        else
            $scope.addedPeople.push(person);
    };
    $scope.removePerson = function (person) {
        var addedPeopleIndex = $scope.addedPeople.idIndexOf("employeeId", person.employeeId);
        $scope.addedPeople.splice(addedPeopleIndex, 1);
        if ($scope.people.length > 0) {
            var peopleIndex = $scope.people.idIndexOf("employeeId", person.employeeId);
            $scope.people[peopleIndex].added = false;
        }
    }
}