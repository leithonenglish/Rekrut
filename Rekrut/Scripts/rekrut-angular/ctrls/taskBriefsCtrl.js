﻿ctrls.taskBriefsCtrl = function ($rootScope, $scope, $http, $compile, $timeout, $sce, FileUploader) {
    //functions
    function getStatusCount(element, index, array) {
        return element.id
    }

    $scope.inIt = function () {
        $rootScope.getUser(function () {
            $http.get('/taskBriefs/getStatusSummary?employeeId=' + $rootScope.person.employeeId).then(function (response) {
                $scope.setStatusSummary(response.data);
                $rootScope.modal = false;
                $rootScope.pageLoading = false;
            });
        });
    }
    $scope.setStatusSummary = function (data) {
        $scope.status = {
            active: data.findByValue("id", "a", "count") | 0,
            completed: data.findByValue("id", "d", "count") | 0,
            processing: data.findByValue("id", "p", "count") | 0,
            closed: data.findByValue("id", "c", "count") | 0,
            overdue: data.findByValue("id", "o", "count") | 0
        }
    };
    $scope.daysLeft = function (days, status) {
        if (status == "Completed" || status == "Closed")
            return status;
        return days == 1 ? "Tomorrow" : days == "0" ? "Today" : days > 1 ?
            days + " days to go" : (days * -1) + " day" + (days == -1 ? "" : "s") + " ago";
    }
    $scope.renderHtml = function (html) {
        return $sce.trustAsHtml(html);
    }
    $scope.statusChange = function (status) {
        $scope.filter[status] = !$scope.filter[status];
        $scope.filterChange();
    }
    $scope.changeStatus = function (task, status, reverse) {
        var data = {
            id: task.id,
            employeeId: $rootScope.person.employeeId,
            status: status,
            reverse: reverse
        }
        $http.post('/taskBriefs/updateStatus', data).then(function (response) {
            $scope.setStatusSummary(response.data.statusSums);
            $rootScope.tasks = response.data.taskCount;
            $timeout(function () {
                if ($scope.fromASearch)
                    $scope.filterChange();
            }, 200);
        });
    }
    $scope.reset = function (all) {
        $scope.task = {
            start: {
                date: '',
                time: '',
                timeOn: false
            },
            end: {
                date: '',
                time: '',
                timeOn: false
            },
            title: '',
            source: "dev",
            project: '',
            priority: 0,
            description: '',
            assignedTo: ''
        }
        $scope.files = [];
        if (all) {
            $scope.saving = false;
            $scope.newTag = "";
            $scope.tags = [];
            $scope.tasks = [];
            $scope.fromASearch = false;
            $scope.loading = false;
            $scope.team = [];
            $scope.projects = [];
            $scope.uploader = new FileUploader();
            $scope.filter = {
                completed: false,
                closed: false,
                active: false,
                processing: false,
                overdue: false
            };
        }
    }
    $scope.projectChange = function () {
        $scope.team = $scope.projects.filter(function (obj) { return obj.projectId == $scope.task.project })[0].members;
        $scope.task.assignedTo = $rootScope.person.employeeId
    }
    $scope.setupTime = function (timeType) {
        if (timeType == 'start') {
            $scope.task.start.timeOn = !$scope.task.start.timeOn;
        }
        else {
            $scope.task.end.timeOn = !$scope.task.end.timeOn;
        }
        $timeout(function () {
            $('.rekrut-modal .time').timepicker({
                showInputs: false
            });
        }, 500);
    }
    $scope.fixDates = function (timeType) {
        if ($scope.task.start.timeOn && $scope.task.end.time) {
            var startTime = moment($scope.task.start.time, 'hh:mm A');
            var endTime = moment($scope.task.end.time, 'hh:mm A');
            if (!startTime.isBefore(endTime)) {
                if (timeType == 'start')
                    $scope.task.end.time = $scope.task.start.time;
                else
                    $scope.task.start.time = $scope.task.end.time;
            }
        }
    }
    $scope.createOrUpdateTask = function (id) {
        var url = "/taskbriefs/getTaskParamsForUser";
        var data = { employeeId: $rootScope.person.employeeId };
        if (id.length > 0) {
            url = "/taskbriefs/editTask";
            data = { id: id, employeeId: $rootScope.person.employeeId };
        }
        $http.post(url, data).success(function (response) {
            if (response.taskParams.length > 0) {
                $rootScope.modal = true;
                $http.get("/taskbriefs/createTask").success(function (html) {
                    $("body").append($compile(html)($scope));
                    $scope.projectNames = response.taskParams.map(function (obj) { return { name: obj.projectName, id: obj.projectId } });
                    $scope.projects = response.taskParams;
                    if (id.length > 0) {
                        $scope.task = response.task;
                    }
                    else {
                        $scope.task.project = $scope.projectNames[0].id;
                        $scope.task.assignedTo = $rootScope.person.employeeId;
                    }
                    $scope.team = $scope.projects.filter(function (obj) { return obj.projectId == $scope.task.project })[0].members;
                    setTimeout(function () {
                        $scope.editor = CKEDITOR.replace('description');
                        $(".rekrut-modal .input-daterange").datepicker({ startDate: moment().format('DD/MM/YYYY'), format: 'dd/mm/yyyy' });
                        $(".rekrut-modal .time").timepicker({
                            showInputs: false
                        });
                        $('.rekrut-modal form .inner').perfectScrollbar();
                    }, 200);
                });
            }
            else {
                $rootScope.showAlert("Unfortunately...",
                    "You cannot Create any tasks because no projects been assigned to you.");
            }
        });
    };
    $scope.saveTask = function (event, form) {
        if (!form.$invalid) {
            $scope.saving = true;
            $scope.task.description = $("<div>").text($scope.editor.getData()).html();
            $.each($scope.uploader.queue, function (k, v) {
                $scope.files.push(v._file)
            });
            $http({
                method: 'post',
                url: '/taskbriefs/saveOrUpdate', 
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = createFormDateFromObject(new FormData(), data.task);
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("files[" + i + "]", data.files[i], data.files[i].name);
                    }
                    return formData;
                },
                data: { task: $scope.task, files: $scope.files }
            }).success(function (data, status, headers, config) {
                $timeout(function () {
                    $scope.saving = false;
                    $scope.closeTaskScreen(event, form);
                    $scope.setStatusSummary(data.statusSums);
                    $rootScope.tasks = data.taskCount;
                    $timeout(function () {
                        if ($scope.fromASearch)
                            $scope.filterChange();
                    }, 200);
                }, 1000);
            }).
            error(function (data, status, headers, config) {
                $timeout(function () {
                    $scope.saving = false;
                }, 200);
            });
        }
    };
    $scope.deleteTask = function (task) {
        $http.post('/taskBriefs/delete', { id: task.id, employeeId: $rootScope.person.employeeId }).then(function (response) {
            $scope.setStatusSummary(response.data.statusSums);
            $rootScope.tasks = response.data.taskCount;
            $timeout(function () {
                if ($scope.fromASearch)
                    $scope.filterChange();
            }, 200);
        });
    }
    $scope.filterChange = function () {
        $scope.loading = true;
        if (propsHasValueCount($scope.filter, true) > 0) {
            $http.post('/taskBriefs/getTasks', { employeeId: $rootScope.person.employeeId, filterModel: $scope.filter }).then(function (response) {
                $scope.tasks = response.data;
                $scope.fromASearch = true;
                $scope.loading = false;
                $timeout(function () {
                    $('#searchResults').perfectScrollbar('update');
                }, 1000);
            });
        }
        else {
            $scope.fromASearch = false;
            $scope.loading = false;
            $scope.tasks = [];
        }
    };
    $scope.closeTaskScreen = function (event, form) {
        $rootScope.closeRekrutModal(event, form, function () {
            $scope.reset(false);
        });
    };
    $scope.statuses = {
        a: "Active",
        d: "Completed",
        c: "Closed",
        o: "Overdue",
        p: "Processing"
    };
    $scope.priorities = [
        { name: "High", value: 3 },
        { name: "Medium", value: 2 },
        { name: "Low", value: 1 },
        { name: "None", value: 0 },
    ];
    $scope.sources = [
        { name: "Production", value: "prod" },
        { name: "Developement", value: "dev" },
        { name: "Quality Assurance", value: "qa" }
    ];
    $scope.reset(true);
    $scope.inIt();
}
rekrutApp.controller(ctrls);