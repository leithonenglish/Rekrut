﻿ctrls.mainCtrl = function ($rootScope, $scope, $http, $uibModal, $timeout) {
    $scope.$on('$viewContentLoaded', function () {
        $('select').select2({ minimumResultsForSearch: -1 });
        $('#searchFilter .filter-holder, #searchResults').perfectScrollbar();
    });
    $scope.logout = function (event) {
        var elm = $(event.target);
        var form = elm.parents("form");
        $.ajax({
            type: 'post',
            url: form.attr("method"),
            data: form.serialize(),
            success: function () {
                $window.location.reload();
            }
        });
    }
    $rootScope.showAlert = function (title, msg) {
        $uibModal.open({
            animation: true,
            templateUrl: 'alertModal.html',
            controller: 'alertModalCtrl',
            windowClass:'alert-modal',
            size: 'sm',
            resolve: {
                details: function () {
                    return { msg: msg, title: title };
                }
            }
        });
    }
    $rootScope.getUser = function (callback) {
        $rootScope.modal = true;
        $rootScope.pageLoading = true;
        if ($rootScope.person != null) {
            if (typeof callback == 'function')
                callback();
        }
        else {
            $http.get('/auth/userinfo').success(function (response) {
                $rootScope.person = response.person;
                $rootScope.projects = response.projects;
                $rootScope.tasks = response.tasks;
                if (typeof callback == 'function')
                    callback();
            });
        }
    };
    $rootScope.closeRekrutModal = function (event, form, callback) {
        form.$setUntouched();
        form.$setPristine();
        var elm = $(event.currentTarget).parents(".rekrut-modal");
        elm.toggleClass("slideInDown slideOutUp");
        if (typeof callback === 'function') {
            $timeout(function () {
                $rootScope.modal = false;
                elm.remove();
                callback();
            }, 500);
        }
    };
    $rootScope.closeGlobalModal = function () {
        $rootScope.closed = true;
        $timeout(function () {
            $rootScope.modal = false;
            $rootScope.url = "";
            $rootScope.closed = false;
        }, 500);
    }
    $rootScope.getUser(null);
}
ctrls.alertModalCtrl = function ($scope, $uibModalInstance, details) {
    $scope.msg = details.msg;
    $scope.title = details.title;
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}
rekrutApp.controller(ctrls);

/* jqeury */
$(document).ready(function () {
    $(document).on("click", ".modal-tabs button:not(.active), .tabs-nav button:not(.active)", function (e) {
        var tabs = $(".modal-tabs button, .tabs-nav button");
        tabs.removeClass("active");
        var self = $(this);
        var index = tabs.index(self);
        var parent = self.parents(".rekrut-modal, .with-tabs");
        var holder = parent.find(".tab-content-holder");      
        var tabContents = holder.find(".tab-content");
        var activeTab = holder.find(".tab-content.active");
        var oldContentIndex = tabContents.index(activeTab)
        tabContents.eq(index).addClass("active");
        holder.animate({ scrollLeft: index * holder.width() + 5 }, 700, "easeInOutBack", function () {
            parent.find("form .inner").scrollTop(0);
            tabContents.eq(oldContentIndex).removeClass("active");
        });
        self.addClass("active");
        e.preventDefault();
    });
});