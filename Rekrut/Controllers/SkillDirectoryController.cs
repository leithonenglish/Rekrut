﻿using Rekrut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading.Tasks;
using Rekrut.Models.Entities;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace Rekrut.Controllers
{
    [Authorize]
    public class SkillDirectoryController : Controller
    {
        // GET: SkillPool
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> GetDepartmentSummary()
        {
            var db = new RekrutContext();
            var departmentsSums =  await db.People.Aggregate().Group(x => x.Department, g => new {
                id = g.Key,
                count = g.Count()
            }).ToListAsync();
            var departments = await db.Departments.Find(new BsonDocument()).ToListAsync();
            var data = departmentsSums.Join(departments, x => x.id, y => y.Id, (x, y) => new {
                id = y.Id,
                name = y.Name,
                count = x.count
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetPeople(SkillPoolFilter param)
        {
            var db = new RekrutContext();
            var filter = new BsonDocument();
            var projects = await db.Projects.Find(new BsonDocument("team", new BsonDocument("$exists", true))).ToListAsync();
            if (param.DepartmentId.HasValue)
            {
                if (param.DepartmentId.Value != -1)
                    filter.Add("department", param.DepartmentId.Value);
            }
            if (param.Tags != null) {
                var tagsArray = new BsonArray();
                foreach (var tag in param.Tags)
                    tagsArray.Add(new Regex(tag, RegexOptions.IgnoreCase));
                filter.Add("$or", new BsonArray().Add(new BsonDocument("skills", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", tagsArray)))))
                .Add(new BsonDocument("languages", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", tagsArray)))))
                .Add(new BsonDocument("tools", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", tagsArray))))));
            }
            if (!param.Everyone)
            {
                filter.Add("hasFamily", param.HasFamily);
                filter.Add("religion", new BsonDocument("$exists", param.IsReligious));
                var ids = new BsonArray();
                var employeeIds = new List<int>();
                foreach(var project in projects)
                {
                    foreach (var member in project.Team.Members)
                        employeeIds.Add(member.EmployeeId);
                }
                foreach (var id in employeeIds.Distinct())
                    ids.Add(Convert.ToInt16(id));
                filter.Add("employeeId", new BsonDocument((param.OnProject ? "$in" : "$nin"), ids));
            }
            filter.Add("$and", new BsonArray().Add(new BsonDocument("yearsOfExperience", new BsonDocument("$lte", param.ExperienceTo)))
                .Add(new BsonDocument("yearsOfExperience", new BsonDocument("$gte", param.ExperienceFrom))));
            var persons = await db.People.Find(filter).ToListAsync();
            var departments = await db.Departments.Find(new BsonDocument()).ToListAsync();
            var data = persons.Join(departments, x => x.Department, y => y.Id, (x, y) => new {
                employeeId = x.EmployeeId,
                firstName = x.FirstName,
                lastName = x.LastName,
                jobTitle = x.JobTitle,
                department = departments.FirstOrDefault(d=> d.Id == x.Department).Name,
                sex = x.Gender.ToLower() == "m" ? "male" : "female",
                experience = x.YearsOfExperience,
                img = x.Img
            }).ToList();
            var jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}