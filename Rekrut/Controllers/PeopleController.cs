﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Rekrut.Models;
using Rekrut.Models.Entities;
using Rekrut.Models.Reusables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rekrut.Controllers
{
    public class PeopleController : Controller
    {
        // GET: People
        [HttpPost]
        public async Task<JsonResult> GetPeopleForProject(string field, string search)
        {
            var db = new RekrutContext();
            var filter = new BsonDocument();
            List<int> departmentIds = new List<int>();
            if (field == "department")
            {
                var department = await db.Departments.Find(x => x.Name.ToLower().Contains(search.ToLower()))
                    .Project(x => new { x.Id, x.Name }).FirstOrDefaultAsync();
                filter.Add("department", department.Id);
                departmentIds.Add(department.Id);
            }
            else if (field == "skills")
            {
                var parts = search.Trim().Split(',');
                var partArray = new BsonArray();
                foreach (var part in parts)
                {
                    if (!string.IsNullOrEmpty(part))
                        partArray.Add(new Regex(part, RegexOptions.IgnoreCase));
                }
                filter.Add("$or", new BsonArray().Add(new BsonDocument("skills", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", partArray)))))
                .Add(new BsonDocument("languages", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", partArray)))))
                .Add(new BsonDocument("tools", new BsonDocument("$elemMatch", new BsonDocument("name", new BsonDocument("$in", partArray))))));
            }
            else
                filter.Add(field, new Regex(search, RegexOptions.IgnoreCase));
            var people = await db.People.Find(filter).Project(x => new
            {
                employeeId = x.EmployeeId,
                firstName = x.FirstName,
                lastName = x.LastName,
                jobTitle = x.JobTitle,
                department = x.Department,
                experience = x.YearsOfExperience,
                imgUrl = x.Img == null ? "Content/images/" + (x.Gender == "M" ? "guy" : "girl") + "-face.png" : x.Img
            }).ToListAsync();
            var employeeIds = people.Select(x => x.employeeId).ToList();
            var projectsForUsers =  await db.Projects.Aggregate().Match(x=> x.Team.Members.Any(y=> employeeIds.Contains(y.EmployeeId)))
                .Project(new BsonDocument { { "_id", 0 }, { "team", 1 } })
                .Unwind("team.members").Group<AggregateCount>(new BsonDocument
                {
                    {"_id", "$team.members.employeeId" },
                    {"count", new BsonDocument{ { "$sum", 1 }  } }
                }).ToListAsync();
            if(departmentIds.Count < 1)
            {
                departmentIds = people.Select(x => x.department).ToList();
            }
            var departments = await db.Departments.Find(x => departmentIds.Contains(x.Id)).ToListAsync();
            var candidates = people.Join(departments, x=> x.department, y=> y.Id, (x, y)=> new {
                person = x,
                departments = y
            }).GroupJoin(projectsForUsers, x => x.person.employeeId, y => y._id, (x, y) => new
            {
                employee = x,
                project = y
            }).SelectMany(x=> x.project.DefaultIfEmpty(), (x, y)=> new
            {
                x.employee.person.employeeId, x.employee.person.firstName,
                x.employee.person.lastName, x.employee.person.jobTitle,
                x.employee.person.experience, x.employee.person.imgUrl,
                department = x.employee.departments.Name,
                projects = y == null ? 0 : y.count,
                added = false
            }).ToList();
            return Json(candidates, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public async Task<JsonResult> GetPerson(int employeeId)
        {
            var db = new RekrutContext();
            var person = await db.People.Find(x => x.EmployeeId == employeeId).FirstOrDefaultAsync();
            var department = await db.Departments.Find(x => x.Id == person.Department).Project(x=> x.Name).FirstOrDefaultAsync();
            var employee = new 
            {
                person.Address,
                person.CareerPath,
                person.FirstName,
                person.LastName,
                person.JobTitle,
                Department = department,
                person.Skills,
                person.Email,
                person.Phone,
                EmployeeId = employeeId,
                person.Religion,
                person.Gender,
                person.HasFamily,
                person.Extension,
                Img = person.Img == null ? "Content/images/" + (person.Gender == "m" ? "guy" : "girl") + "-face.png" : person.Img
            };
            return Json(employee, JsonRequestBehavior.AllowGet);
        }
    }
}