﻿using System.Web.Mvc;

namespace Rekrut.Controllers
{
    public class PageNotFoundController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}