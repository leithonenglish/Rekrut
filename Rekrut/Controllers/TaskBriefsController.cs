﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rekrut.Models;
using Rekrut.Models.Reusables;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rekrut.Controllers
{
    [Authorize]
    public class TaskBriefsController : Controller
    {
        // GET: TaskBriefs
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateTask()
        {
            return View();
        }
        public async Task<JsonResult> UpdateStatus(string id, int employeeId, string status, bool reverse)
        {
            var db = new RekrutContext();
            var update = Builders<Models.Entities.Task>.Update.Set(x => x.Status, status);
            if (status == "c" && !reverse)
                update.Set(x => x.CompletionDate, DateTime.Now);
            if (reverse)
                update.Unset(x => x.CompletionDate);
            try {
                var result = await db.Tasks.UpdateOneAsync(x => x.Id == id && x.AssignedTo == employeeId, update);
                var statusSums = await GetTasksSummary(employeeId);
                return Json(new { result = result.ModifiedCount, statusSums = statusSums, taskCount = statusSums.Sum(x => x.count) });
            }
            catch (Exception e)
            {
                return Json(new { result = -1, msg = e.InnerException.InnerException.Message });
            }
        }
        [HttpPost]
        public async Task<JsonResult> Delete(string id, int employeeId)
        {
            var db = new RekrutContext();
            try {
                var result = await db.Tasks.DeleteOneAsync(x => x.Id == id && x.AssignedTo == employeeId);
                var statusSums = await GetTasksSummary(employeeId);
                return Json(new { result = result.DeletedCount, statusSums = statusSums, taskCount = statusSums.Sum(x => x.count) });
            }
            catch (Exception e)
            {
                return Json(new { result = -1, msg = e.InnerException.InnerException.Message });
            }
        }
        public async Task<JsonResult> EditTask(string id, int employeeId)
        {
            var db = new RekrutContext();
            var task = await db.Tasks.Find(x => x.Id == id).Project(x => new TaskViewModel
            {
                id = x.Id,
                assignedTo = x.AssignedTo,
                description = HttpUtility.HtmlDecode(x.Description),
                end = new TaskDateViewModel
                {
                    date = string.Format("{0: dd/MM/yyyy}", x.EndDate),
                    timeOn = !x.ForDayEnd,
                    time = x.ForDayEnd ? "" : string.Format("{0:hh:mm tt}", x.EndDate)
                },
                start = new TaskDateViewModel
                {
                    date = string.Format("{0: dd/MM/yyyy}", x.StartDate),
                    timeOn = !x.ForDayBegining,
                    time = x.ForDayBegining ? "" : string.Format("{0:hh:mm tt}", x.StartDate)
                },
                priority = x.Priority,
                project = x.Project,
                source = x.Source,
                title = x.Title
            }).FirstOrDefaultAsync();
            var taskParams = await GetTaskParams(employeeId);
            return Json(new {task = task, taskParams = taskParams }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> SaveOrUpdate(TaskViewModel taskModel)
        {
            var db = new RekrutContext();
            var oids = new List<string>();
            try {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    var id = await db.GridFS.UploadFromStreamAsync(file.FileName, file.InputStream);
                    oids.Add(id.ToString());
                }
            }
            catch(Exception e)
            {
                return Json(new {result = -1, msg = e.InnerException.Message });
            }
            
            try {
                var newTask = CreateTaskObject(taskModel, oids);
                if (string.IsNullOrEmpty(taskModel.id))
                    await db.Tasks.InsertOneAsync(newTask);
                else
                    await db.Tasks.ReplaceOneAsync(x => x.Id == taskModel.id, newTask);
            }
            catch(Exception e)
            {
                return Json(new { result = -1, msg = e.InnerException.Message });
            }
            var eid = await db.People.Find(x => x.Username == User.Identity.Name).Project(x=> x.EmployeeId).FirstOrDefaultAsync();
            var statusSums = await GetTasksSummary(eid);
            return Json(new { result = 1, statusSums = statusSums, taskCount = statusSums.Sum(x => x.count) });
        }
        [HttpPost]
        public async Task<JsonResult> GetTasks(int employeeId, TaskFilterModel filterModel)
        {
            var db = new RekrutContext();
            var projects = await db.Projects.Find(x => x.Team.Members.Any(y => y.EmployeeId == employeeId)).Project(x => new
            {
                id = x.Id,
                name = x.Name
            }).ToListAsync();
            var filter = new BsonDocument();
            var statuses = new BsonArray();
            filter.Add("assignedTo", employeeId);

            if (filterModel.active)
                statuses.Add(new BsonDocument("status", "a").AddRange(new BsonDocument("endDate", new BsonDocument("$gte", DateTime.Now))));
            if (filterModel.closed)
                statuses.Add(new BsonDocument("status", "c"));
            if (filterModel.processing)
                statuses.Add(new BsonDocument("status", "p"));
            if (filterModel.completed)
                statuses.Add(new BsonDocument("status", "d"));
            if (filterModel.overdue)
            {
                var overdueFilter = new BsonArray();
                overdueFilter.Add(new BsonDocument("status", "a")).Add(new BsonDocument("endDate", new BsonDocument("$lt", DateTime.Now)));
                statuses.Add(new BsonDocument("$and", overdueFilter));
            }
            if (statuses.Any())
                filter.Add("$or", statuses);
            var tasks = await db.Tasks.Find(filter).Project(x => new {
                id = x.Id,
                title = x.Title,
                description = HttpUtility.HtmlDecode(x.Description),
                source = Constants.TaskSource[x.Source],
                project = projects.FirstOrDefault(p=> p.id == x.Project),
                priority = Constants.TaskPriority[x.Priority],
                status = x.Status == "a" && x.EndDate.DaysLeft() < 0? "Overdue" : Constants.TaskStatus[x.Status],
                due = x.EndDate.DaysLeft(),
                time = x.ForDayEnd ? "" : string.Format("{0:hh:mm tt}", x.EndDate)
            }).ToListAsync();
            return Json(tasks, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetStatusSummary(int employeeId)
        {
            var statusSums = await GetTasksSummary(employeeId);
            return Json(statusSums, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> GetTaskParamsForUser(int employeeId)
        {
            var taskParams = await GetTaskParams(employeeId);
            return Json(new { taskParams = taskParams }, JsonRequestBehavior.AllowGet);
        }
        #region Special Fetches
        public async Task<List<TaskStatusSummary>> GetTasksSummary(int employeeId)
        {
            var db = new RekrutContext();
            var overdueTasks = await db.Tasks.Find(x => x.AssignedTo == employeeId && !(new string[] { "d", "c" }).Contains(x.Status) && x.EndDate < DateTime.Now).Project(x => x.Id).ToListAsync();

            var statusSums = await db.Tasks.Aggregate().Match(x => x.AssignedTo == employeeId && !overdueTasks.Contains(x.Id)).Group(x => x.Status, g => new TaskStatusSummary
            {
                id = g.Key,
                count = g.Count()
            }).ToListAsync();
            statusSums.Add(new TaskStatusSummary { id = "o", count = overdueTasks.Count });
            return statusSums;
        }
        public async Task<List<TaskParam>> GetTaskParams(int employeeId)
        {
            var db = new RekrutContext();
            var projects = await db.Projects.Find(new BsonDocument("team.members", new BsonDocument("$elemMatch", new BsonDocument("employeeId", employeeId)))).Project(x => new
            {
                id = x.Id,
                name = x.Name,
                team = x.Team.Members.AsEnumerable().Select(y => y.EmployeeId)
            }).ToListAsync();
            var bArray = new BsonArray();
            var taskParams = new List<TaskParam>();
            foreach (var project in projects)
            {
                var taskParam = new TaskParam
                {
                    projectId = project.id,
                    projectName = project.name,
                    members = await db.People.Find(x => project.team.Contains(x.EmployeeId))
                    .Project(x => new TaskParamTeamMember
                    {
                        id = x.EmployeeId,
                        name = x.FirstName + " " + x.LastName
                    }).ToListAsync()
                };
                taskParams.Add(taskParam);
            }
            return taskParams;
        }
        public Models.Entities.Task CreateTaskObject(TaskViewModel taskModel, List<string> oids)
        {
            var endDate = DateTime.Now;
            var startDate = DateTime.Now;
            DateTime.TryParse(taskModel.end.date, out endDate);
            DateTime.TryParse(taskModel.start.date, out startDate);
            var newTask = new Models.Entities.Task
            {
                AssignedBy = User.Identity.Name,
                Description = taskModel.description,
                EndDate = taskModel.end.timeOn ? endDate.Add(taskModel.end.time.ToTimeSpan()) : endDate,
                DateAssigned = DateTime.Now,
                CompletionDate = null,
                ForDayEnd = !taskModel.end.timeOn,
                ForDayBegining = !taskModel.start.timeOn,
                Priority = taskModel.priority,
                Source = taskModel.source,
                Title = taskModel.title,
                Status = "a",
                AssignedTo = taskModel.assignedTo,
                Project = taskModel.project,
                StartDate = taskModel.start.timeOn ? startDate.Add(taskModel.start.time.ToTimeSpan()) : startDate,
                Files = oids
            };
            if (!string.IsNullOrEmpty(taskModel.id))
                newTask.Id = taskModel.id;
            return newTask;
        }
        #endregion
    }
}