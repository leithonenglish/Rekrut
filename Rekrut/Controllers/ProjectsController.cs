﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rekrut.Models;
using Rekrut.Models.Reusables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rekrut.Controllers
{
    public class ProjectsController : Controller
    {
        // GET: Projects
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateOrUpdate()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> GetProjects(int employeeId, string sortBy, string order, ProjectFilter param)
        {
            var db = new RekrutContext();
            var filter = new BsonDocument();
            var orAndArray = new BsonArray();
            if (param.assignedOnly)
                orAndArray.Add(new BsonDocument("team.members", new BsonDocument("$elemMatch", new BsonDocument("employeeId", employeeId))));

            if (param.difficulties != null)
                orAndArray.AddInArrayFilter("difficulty", param.difficulties);

            if (param.statuses != null)
                orAndArray.AddInArrayFilter("status", param.statuses);

            if (param.types != null)
                orAndArray.AddInArrayFilter("type", param.types);

            if (param.priorities != null)
                orAndArray.AddInArrayFilter("priority", param.priorities);
            
            if(orAndArray.Count > 0)
                filter.Add("$and", orAndArray);

            var projects = await db.Projects.Find(filter).Project(x => new
            {
                id = x.Id,
                client = x.Client,
                name = x.Name,
                type = Constants.ProjectType[x.Type],
                team = x.Team.Members.Count,
                assignedToMe = x.Team.Members.Any(y=> y.EmployeeId == employeeId),
                subProjects = x.Links == null ? 0 : x.Links.Count,
                status = Constants.ProjectStatus[x.Status],
                priority = Constants.ProjectPriority[x.Priority],
                difficulty = Constants.ProjectDifficulty[x.Difficulty],
            }).ToListAsync();
            var tasks = await db.Tasks.Aggregate().Project(x => new
            {
                x.Project,
                x.Id
            }).Group(x => x.Project, g => new { id = g.Key, count = g.Count() }).ToListAsync();
            var projectsWithTasks = projects.GroupJoin(tasks, x => x.id, y => y.id, (x, y) => new
            {
                project = x,
                task = y
            }).SelectMany(x => x.task.DefaultIfEmpty(), (x, y) => new
            {
                x.project.id, x.project.client, x.project.name, x.project.assignedToMe,
                x.project.type, x.project.team, x.project.subProjects, x.project.status,
                x.project.priority, x.project.difficulty, tasks = y == null ? 0 : y.count
            });
            return Json(projectsWithTasks, JsonRequestBehavior.AllowGet);
        }
    }
}